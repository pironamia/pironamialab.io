import React from 'react';

// Path to the logo file on your project
import piromaniaLogo from './logo.png';

const Logo = () => (
    <img src={piromaniaLogo} alt="Piromania logo" style={{
        maxWidth: 180,
        maxHeight: 70
    }}/>
);

export default Logo;