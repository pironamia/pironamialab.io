---
title: The Game Environment
description: The Game Environment subsystem for the game.
disableTableOfContents: false
---

This subsystem is everything to do with the game itself. From all the data handling, generating, and passing, to game balance and logic.
This subsystem was designed so it could be plugged into any front end or engine with a very easy to use API, and extensible features for future development.

*For demonstrational purposes of this subsystem, images with a GUI are shown, however the user interface could be entirely different if another was made yet the same game environment code was plugged into it.*

In general, this subsystem can be broken down into sub-components:

### Events, choices, and actions

This flexible component is used to create any events that the player might encounter on their journey.
An event holds information about the event, which has subsequent choices that can be chosen.
Each choice has a relevant title and description for showing exactly what taking the choice will do, and are formatted with a TextFormatter for style.
A choice can initiate any number of actions sequentially, which have full access to the game environment. These can be modified easily to include live data, such as ship HP, or a random value or effect if need be.
Events, choices, and actions can all be described with JSON, and are split into three categories:

* Good events
* Neutral events
* Bad events

all of which can occur whilst sailing randomly, but only ever one event at a time. (Excluding follow-on Events).

---

### Islands, stores, & routes

#### Islands

In the game, there are 5 islands. These are randomly generated on a grid.
Each island is made up of:

* A name
* A description
* An image
* A store

These are decided by a random pick from a few JSON files.
A generated visual of this map can be accessed through the main screen once the game has started.

![A picture showing the map of islands in the game](../files/island-map.png)

#### Stores

Stores are essential to the game. You purchase and sell items to and fro different shops to make a profit as a trader.
Shops that aren't explicitly defined in the store.json file will be randomly generated, however those that are defined will *always* be in the game.
A store is made of a few key elements:

* A name based on the type of shop
* A buying type
* A selling type
* A personality
* An inventory

The buying / selling type of the store will affect the prices of both buying and selling separately.
For example, a store that wants to sell gems will typically sell them cheaper, but a store that wants to buy gems will typically buy them for more.

The personality of a store will affect prices as well, adding great variance between the shop's prices to encourage exploration for more profit.

![A picture of an example shop trading](../files/shop-example.png)

#### Routes

A route is used to connect islands together. Islands must always be connected by at least one route, and at most five routes.
Because of this, We've added variation to how routes are generated as to incentivize players choosing different routes for different strategies.
A player looking to get a high score might play very riskily for example, choosing routes with high risk but a fast travel time.
This also plays into the ship and its relevant statistics. One particular ship might be more risky than another, at the tradeoff of being able to carry more cargo.
Routes were key to having varied playstyles for different players, and ensuring that actions have consequences.

![A picture of the route planner in action](../files/route-example.png)

---

### Items

From the beginning of the project, it was decided that Items were going to have a very strong precedence.
Not only are they what are traded, but we've also had them have major impacts on the game.
Most items consist of:

* A name
* A description
* An image
* A base value
* A type which can be one of the following:
    * Drink
    * Food
    * Gem
    * Upgrade
* A size used for determining how much cargo space the item takes up

Stores will look at the base value of items before adjusting the prices in the ways mentioned up in the Store section.

**Upgrade items** however are quite special. They work exactly like normal items, but with a few key differences:

* Upgrade statistics, or how they modify the ship in these ways:

    * Hitpoint maximum
    * Risk multiplier
    * Damage potential
    * Cargo capacity
    * Travel multiplier
    
    *All of which are described under the Ship section later.*

* Upgrade types, or how they can be applied to the ship, including:

    * Hull
    * Steering
    * Weaponry
    * Cargo
    * Sail
    
    *More information on these also in the Ship section later.*

* Additive or multiplicative to determine if the modifiers should be added or multiplied to the existing statistics of the ship

All items are tracked in inventories, and when they are sold they are sent to a sold ledger for display later.

![A picture with an example of a purchased item](../files/item-example.png)

All of these items are described in a JSON file, and more can easily be added with the JSON guide. See Data serialisation for more details.

---

### Inventories

In order to actually store anything, stores, the sold ledger, and ships have inventories tied to them.
Inventories consist of:

* A capacity
* All the items inside the inventory
* The total worth of all items in the inventory based on their base value

Inventories can be initialized in many ways, for example with stores they are initialized with an infinite capacity and a random collection of items.
This random collection is based on key statistics from the shop, such as its buying/selling type.

Inventories can be modified with simple adding/removing, changing of capacity, and sorting based on the following item descriptors:

* Base value
* Size
* Name
* Type
* ID

---

### Ships

Ships are fundamental to the game. The player has a choice between four random ships at the start of the game, and will most likely aim their entire play style around what they choose.
The Ship is also the player in this game, and they are responsible for everything surrounding it. To differentiate the ships, the ships have these following factors:

* A name
* A description
* An image
* The amount of crew aboard a ship
* The statistics of the ship
* The upgrade slots of the ship
* A base travel speed

All these factors have drastic game play changes. A ship with less crew will cost less to sail, but will more than likely be slower as well.
A ship with more cargo space might be able to handle bulk trading better, but will cost you time as the speed is lower.
Or a ship with strong starting statistics but very few upgrade slots makes the early game strong, only to taper off towards the latter thirds of the game.

Because choosing a ship is so integral, the player gets given all of the information of the ship when choosing so they can decide between the four ships given to them.
The different ships should make the players employ different strategies, and have an impact of a more re-playable game.

Ships can be upgraded, with various upgrade items that can be purchased. These can modify the ships stats in many ways, at a cost of using an upgrade slot.
An upgrade slotting system was derived in inspiration to games such as [Stellaris][stellaris_link] or [Endless Space 2][endless_space_2_link], where you can apply different upgrades to the ships to make them different.
I found this solution rather elegant, and as such employed it in this game. It provides a very nice balance and important choices for what upgrade should go where, as well as limiting as to not become overpowered.

![An example of a ship being upgraded with a pirate flag on the hull](../files/ship-upgrade-example.png)

---

### Stats

Stats are employed everywhere in the game. Upgrade items will have stats which can be applied to the ship, and ships have stats. These stats are defined as follows:

* Hitpoint maximum
* Risk multiplier
* Damage potential
* Cargo capacity
* Travel multiplier

These stats are reused generally whenever a calculation is done. For example, route risk will take into account the risk multiplier of a ship, and adjust as such.
Damage potential is used to calculate likelihood of winning against pirates as well as hitpoint maximum.

---

### Data serialisation

To initialise all of the data in the game, a genericised [JSON manager][json_manager_link] was created and used.
This could plug in all of our type adapters for instances such as events / choices / actions or observables that needed to be serialised in a special way.
This class uses GSON to handle all of the JSON files that describe key objects in the game. This allows us to use this generic JSON manager and apply it to all the JSON files.
It can also pick out random data if need be. A link to the JSON Readme file that can be used by anyone can be found [here.][JSONReadMe_link] 

---

### Text formatters

Text formatters were used throughout the development process on the rendering side. It can take an input provided by the Game Environment following special syntax, and replace it with stylisation.

[stellaris_link]: https://www.stellaris.com/
[endless_space_2_link]: https://www.endless-space.com/
[json_manager_link]: https://gitlab.com/pironamia/piromania/-/blob/master/piro-app/src/main/java/nz/piromania/JsonManager.java
[JSONReadMe_link]: /files/JSONREADME