module.exports = {
  siteMetadata: {
    siteTitle: `Piromania Documentation`,
    defaultTitle: `Piromania Documentation`,
    siteTitleShort: `Piromania Documentation`,
    siteDescription: `A Java + Swing desktop application written by two students as part of a university course.`,
    siteUrl: `https://pironamia.gitlab.io/`,
    siteAuthor: `@tfinlay @NobodyKiller`,
    siteImage: `/banner.png`,
    siteLanguage: `en-NZ`,
    themeColor: `#8257E6`,
    basePath: `/`,
    pathPrefix: `/`
  },
  flags: { PRESERVE_WEBPACK_CACHE: true },
  plugins: [
    {
      resolve: `@rocketseat/gatsby-theme-docs`,
      options: {
        configPath: `src/config`,
        docsPath: `src/docs`,
        repositoryUrl: `https://gitlab.com/pironamia/pironamia.gitlab.io`,
        baseDir: ``,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Rocketseat Gatsby Themes`,
        short_name: `RS Gatsby Themes`,
        start_url: `/`,
        background_color: `#ffffff`,
        display: `standalone`,
        icon: `static/favicon.png`,
      },
    },
    `gatsby-plugin-sitemap`,
    // {
    //   resolve: `gatsby-plugin-google-analytics`,
    //   options: {
    //     trackingId: `YOUR_ANALYTICS_ID`,
    //   },
    // },
    `gatsby-plugin-remove-trailing-slashes`,
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://pironamia.gitlab.io`,
      },
    },
    `gatsby-plugin-offline`,
  ],
};
