# Piromania Docs

This is the Gatsby documentation site for Piromania.

To get started, run the following commands:
```bash
$ yarn install
$ yarn start
```

NB: This project uses [yarn](https://yarnpkg.com/)
